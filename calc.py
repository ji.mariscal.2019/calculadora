def sumar(a, b):
    return a + b

def restar(a, b):
    return a - b

if __name__ == "__main__":
    # Sumar 1 y 2
    resultado_suma1 = sumar(1, 2)
    print("1 + 2 =", resultado_suma1)

    # Sumar 3 y 4
    resultado_suma2 = sumar(3, 4)
    print("3 + 4 =", resultado_suma2)

    # Restar 5 de 6
    resultado_resta1 = restar(5, 6)
    print("5 - 6 =", resultado_resta1)

    # Restar 7 de 8
    resultado_resta2 = restar(7, 8)
    print("7 - 8 =", resultado_resta2)
